package com.isna_19102169.praktikum14.model

data class QuoteResponse(
    val quotes: ArrayList<Quote>)