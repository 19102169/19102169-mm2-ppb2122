package com.isna_19102169.praktikum14.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Token (
    var token: String? = null
): Parcelable