package com.isna_19102169.praktikum14.`interface`

import com.isna_19102169.praktikum14.model.Login
import com.isna_19102169.praktikum14.model.Quote

interface MainView {
    fun showMessage(messsage : String)
    fun resultQuote(data: ArrayList<Quote>)
    fun resultLogin(data: Login)
}